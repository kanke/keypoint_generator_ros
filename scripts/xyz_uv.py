#!/usr/bin/python

import os
import json
import datetime

import rospy
import ros_numpy

import cv2
import tf
import message_filters
import image_geometry
from resource_retriever import get_filename

from sensor_msgs.msg import Image, CameraInfo

class KeypointGenerator(object):
    def __init__(self):

        self.camera_input_topic = rospy.get_param("~camera_input_topic", "camera_input")
        self.camera_info_topic = rospy.get_param("~camera_info_topic", "camera_info")
        self.output_topic = rospy.get_param("~output_topic", "keypoint_gen_output")
        self.camera_tf_frame = rospy.get_param("~camera_tf_frame", "camera_tf_frame")
        #self.target_tf_frames = rospy.get_param("~target_tf_frames", [""])
        self.img_save_hz = rospy.get_param("~img_save_hz", 10)
        self.img_save = rospy.get_param("~img_save", True)

        keypoint_frames = rospy.get_param("/keypoint_frames", [])
        self.target_tf_frames = []
        for kp_frame in keypoint_frames:
            self.target_tf_frames.append(kp_frame["child"])

        self.data_cnt = 1
        self.msg_cnt = 0
        self.camera_info = None

        image_sub = message_filters.Subscriber(self.camera_input_topic, Image)
        camera_info_sub = message_filters.Subscriber(self.camera_info_topic, CameraInfo)
        self.ts = message_filters.TimeSynchronizer([image_sub, camera_info_sub], 10)
        self.ts.registerCallback(self.ImageCallback)

        self.pcm = image_geometry.PinholeCameraModel()

        self.tf_listener = tf.TransformListener()

        self.output_pub = rospy.Publisher(self.output_topic, Image, queue_size=10)

        # json
        self.data_dir = get_filename("package://keypoint_generator/data", False) + '/' + datetime.datetime.now().strftime('%Y-%m-%d-%H:%M')
        self.img_save_dir = self.data_dir + '/images'
        if self.img_save:
            os.makedirs(self.img_save_dir)
        self.json_save_path = self.data_dir + '/annotation.json'
        self.jsn = {
            "keypoints":self.target_tf_frames,
            "annotations":[],
        }

    def save_json(self):
        if self.img_save:
            with open(self.json_save_path, "w") as f:
                json.dump(self.jsn, f, indent=2)

    def ImageCallback(self, img_msg, info_msg):
        self.msg_cnt += 1
        if self.msg_cnt % self.img_save_hz == 0:
            #update camera information
            self.pcm.fromCameraInfo(info_msg)

            np_img = ros_numpy.image.image_to_numpy(img_msg)

            try:
                print("######")
                annotation = {
                    "file_name": str(self.data_cnt).zfill(5) +".png",
                    "keypoints": [],
                    "valid": 1
                }

                frames = self.target_tf_frames
                for frame in frames:
                    (trans, rot) = self.tf_listener.lookupTransform(self.camera_tf_frame, frame, rospy.Time(0))
                    xyz = trans
                    uv = self.pcm.project3dToPixel(xyz)

                    annotation["keypoints"].append({
                        "x":uv[0],
                        "y":uv[1]
                    })

                    uv = tuple(map(int, uv))
                    print(uv)
                    cv2.circle(np_img, center=uv, radius=10, thickness=-1, color=(255,0,0))

                output_msg = ros_numpy.image.numpy_to_image(np_img, encoding="rgb8")
                self.output_pub.publish(output_msg)

                self.jsn["annotations"].append(annotation)

                if self.img_save:
                    img_path = self.img_save_dir + '/' + str(self.data_cnt).zfill(5) +".png"
                    save_img = ros_numpy.image.image_to_numpy(img_msg)
                    save_img = cv2.cvtColor(save_img, cv2.COLOR_BGR2RGB)  
                    cv2.imwrite(img_path, save_img)

                self.data_cnt += 1

            except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException) as e:
                print(e)



if __name__=="__main__":

    rospy.init_node("keypoint_generator")

    kp_generator = KeypointGenerator()

    rospy.spin()

    kp_generator.save_json()
