#!/usr/bin/python

import rospy
import tf
import math

class AxisConverter(object):
    def __init__(self):

        self.original_camera_tf_frame = rospy.get_param("~original_camera_tf_frame", "original_camera_tf_frame")
        self.camera_tf_frame = rospy.get_param("~camera_tf_frame", "camera_tf_frame")
        self.rate = 100

        self.br = tf.TransformBroadcaster()

    def broadcast(self):
        rate = rospy.Rate(self.rate)
        while not rospy.is_shutdown():
            self.br.sendTransform(
                (0,0,0),
                tf.transformations.quaternion_from_euler(-math.pi/2,0,0),
                rospy.Time.now(),
                self.camera_tf_frame,
                self.original_camera_tf_frame
            )

            rate.sleep()

if __name__=="__main__":

    rospy.init_node("axis_converter")

    axis_converter = AxisConverter()

    axis_converter.broadcast()

    rospy.spin()
