#!/usr/bin/python

import rospy
import tf
import math

class AxisConverter(object):
    def __init__(self):

        self.frames = rospy.get_param("/keypoint_frames", [])
        self.rate = 100

        self.br = tf.TransformBroadcaster()

    def broadcast(self):
        rate = rospy.Rate(self.rate)
        while not rospy.is_shutdown():
            for frame in self.frames:
                x,y,z = frame["xyz"]
                R,P,Y = frame["rpy"]
                self.br.sendTransform(
                    (x,y,z),
                    tf.transformations.quaternion_from_euler(R,P,Y),
                    rospy.Time.now(),
                    frame["child"],
                    frame["parent"]
                )

            rate.sleep()

if __name__=="__main__":

    rospy.init_node("axis_converter_target")

    axis_converter = AxisConverter()

    axis_converter.broadcast()

    rospy.spin()
